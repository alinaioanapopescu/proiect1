
//EXERCITIUL 1
let numberOfChildren, partnersName, geographicLocation, jobTitle;
numberOfChildren=3;
partnersName="John";
geographicLocation="America";
jobTitle="programmer";

console.log(`You will be a ${jobTitle} in  ${geographicLocation} and married to ${partnersName} with  ${numberOfChildren} kids.`);


/*
//EXERCITIUL 2
let birthYear, futureYear, possibleAge1, possibleAge2;

 birthYear=2002;
 futureYear=2030;
 possibleAge1=futureYear-birthYear-1;
 possibleAge2=futureYear-birthYear;
 
 console.log(`I will be either ${possibleAge1} or ${possibleAge2} in ${futureYear}`);
*/

/*
//EXERCITIUL 3
let currentAge=20;
let maximumAge=60;
let estimatedAmount=5;

//variabila pentru calcului cantitatii totale
let amount=(maximumAge-currentAge)*365*estimatedAmount;

console.log(`You will need ${amount} to last you until the ripe old age of ${maximumAge}`);
*/

/*
//EXERCITIUL 4
const pi=3.141592;
let radius=3;
let circumference,area;

circumference=2*pi*radius;
area=pi*Math.pow(radius,2);

console.log(`The circumference is ${circumference}`);
console.log(`The area is ${area}`);
*/

/*
//EXERCITIUL 5
let celsiusTemperature=30;

//am adaugat variabila celsiusConverted pentru a face conversia si a nu pierde valoarea initiala in celsius 
let celsiusConverted;

celsiusConverted =celsiusTemperature*1.8+32;
console.log(`${celsiusTemperature} °C is ${celsiusConverted} °F`);

let fahrenheitTemperature=celsiusConverted;
console.log(`${fahrenheitTemperature} °F is ${celsiusTemperature} °C`);
*/

/*
//EXERCITIUL 6
let nameOfSpaceShuttle="Determination";
let shuttleSpeed=17500;
let kmToMars=225000000;
let kmToTheMoon=384400;
let milesPerKilometer=0.621;

let milesToMars=kmToMars*milesPerKilometer;
let hoursToMars=milesToMars/shuttleSpeed;
let daysToMars=hoursToMars/24;

console.log( `nameOfSpaceShuttle will take ${daysToMars} days to reach Mars`);

let milesToMoon=kmToTheMoon*milesPerKilometer;
let hoursToMoon=milesToMoon/shuttleSpeed;
let daysToMoon=hoursToMoon/24;

console.log( `nameOfSpaceShuttle will take ${daysToMoon} days to reach the Moon.`);

*/
