class Animal {
    constructor(name) {
        this.name = name;
    }
}
class Rabbit extends Animal {
    constructor(name) {
        super(name);
        this.created = Date.now();
    }
}

//this.name = name; declansa eroarea deoarece nu se mostenea name si nu exista name curent
let rabbit = new Rabbit("White Rabbit"); // Error: this is not defined
alert(rabbit.name);