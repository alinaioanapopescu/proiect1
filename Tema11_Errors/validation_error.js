class ValidationError extends Error {
    constructor(message) {
        super(message);
        this.name = "ValidationError";
    } 
    Validation(User) {
        let user=JSON.parse(User);
    if (!user.name) {
        throw new ValidationError("Incomplete data: no name");
    }
    if (!user.age) {
        throw new ValidationError("Incomplete data: no age");
    }
    if (!user.name && !user.age) {
        throw new ValidationError("Incomplete data: no name & no age");
    }
    if (typeof user.name !== "string" && typeof user.age !== "number") {
        throw new ValidationError(
            "Incomplete data: name is not a string & age is not a number"
        );
    }
    if (typeof user.name !== "string") {
        throw new ValidationError("Incomplete data: name is not a string");
    }
    if (typeof user.age !== "number") {
        throw new ValidationError("Incomplete data: age is not a number");
    }
    return user;
}
}

let validation = new ValidationError("");

let json2='{"name": "Peter"}';
console.log(validation.Validation(json2));

