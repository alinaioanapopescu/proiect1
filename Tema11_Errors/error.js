  //strings for users
  let json1='{"name": "Peter", "age": 45}';
  let json2='{"name": "Peter"}';
  let json3='{ "age": 22}';

    //name+age introduced
    try {
        let obj = JSON.parse(json1);
        if (!obj.name && !obj.age) {
            throw new SyntaxError("No data");
        }
        else if (!obj.name) {
            throw new SyntaxError("No name!");
        }
        else if (!obj.age) {
            throw new SyntaxError("No age!");
        }

        else console.log(obj);
    }
    catch (err) {
        alert("JSON error: " + err.message);
    }

    //name introduced
    try {
        let obj = JSON.parse(json2);
        if (!obj.name && !obj.age) {
            throw new SyntaxError("No data");
        }
        else if (!obj.name) {
            throw new SyntaxError("No name!");
        }
        else if (!obj.age) {
            throw new SyntaxError("No age!");
        }

        else console.log(obj);
    }
    catch (err) {
        alert("JSON error: " + err.message);
    }

    //age introduced
    try {
        let obj = JSON.parse(json3);
        if (!obj.name && !obj.age) {
            throw new SyntaxError("No data");
        }
        else if (!obj.name) {
            throw new SyntaxError("No name!");
        }
        else if (!obj.age) {
            throw new SyntaxError("No age!");
        }

        else console.log(obj);
    }
    catch (err) {
        alert("JSON error: " + err.message);
    }






