var libButton = document.getElementById('lib-button');
var libIt = function () {
    var storyDiv = document.getElementById("story");
    var noun = document.getElementById("noun").value;
    var adjective = document.getElementById("adjective").value;
    var name = document.getElementById("person").value;
    storyDiv.innerHTML = name+ ' '+'really likes '+ adjective+' ' +noun;
};
libButton.addEventListener('click', libIt);