let playersState1 = document.getElementById("player1");
let playersState2 = document.getElementById("player2");


let cards = document.querySelectorAll(".card");
let cards1 = document.querySelectorAll(".card1");

let timeTag = document.querySelector(".time b");

let flipsTag1_3x4 = document.getElementById("flip-player1-3x4");
let flipsTag2_3x4= document.getElementById("flip-player2-3x4");
let flipsTag1_4x4 = document.getElementById("flip-player1-4x4");
let flipsTag2_4x4= document.getElementById("flip-player2-4x4");

let refreshBtn1 = document.getElementById("details-3x4")
let refreshBtn2 = document.getElementById("details-4x4");

let playerScore1_3x4= document.getElementById("multiplayer-text1-3x4");
let playerScore2_3x4= document.getElementById("multiplayer-text2-3x4");
let playerScore1_4x4= document.getElementById("multiplayer-text1-4x4");
let playerScore2_4x4= document.getElementById("multiplayer-text2-4x4");


let gameMode1 = document.getElementById("3x4");
let gameMode2 = document.getElementById("4x4");

let content1 = document.getElementById("board3");
let content2 = document.getElementById("board4");

let text = document.getElementById("choose-game-mode");
let choose = document.getElementById("go-back-choose-btn");

let minTime = 0;
let timeCount = minTime;

let flips1 = 0;
let flips2 = 0;

let matchedCard = 0;
let disableDeck = false;
let isPlaying = false;
let cardOne, cardTwo, timer;

let score1P1 = 0;
let score1P2 = 0;
let score2P1 = 0;
let score2P2 = 0;

let game1Started = false;
let game2Started = false;

let player1turn;
let player2turn;


const show = elem => {
    elem.classList.remove('hide');
}

const hide = elem => {
    elem.classList.add('hide');
}

//SET FONT COLORS AT THE START
function colorSet(){
    playerScore1_4x4.style.color='white';
    playerScore2_4x4.style.color='white';
    playerScore1_3x4.style.color='white';
    playerScore2_3x4.style.color='white';
}

//3X4 MODE SELECTED
gameMode1.addEventListener("click", () => {
    game1Started = true;
    game2Started = false;
    console.log(game1Started);
    show(content1);
    hide(text);
    show(choose);
    score1P1 = 0;
    score1P2 = 0;
    score2P1 = 0;
    score2P2 = 0;
    playerScore1_3x4.innerHTML = `Matches found : ${score1P1}`;
    playerScore2_3x4.innerHTML = `Matches found : ${score1P2}`;
    player1turn = true;
    player2turn = false;
    shuffleCard();
    colorSet();
})

//4X4 MODE SELECTED
gameMode2.addEventListener("click", () => {
    show(content2);
    hide(text);
    show(choose);
    score1P1 = 0;
    score1P2 = 0;
    score2P1 = 0;
    score2P2 = 0;
    game2Started = true;
    game1Started = false;
    playerScore1_4x4.innerHTML = `Matches found : ${score2P1}`;
    playerScore2_4x4.innerHTML = `Matches found : ${score2P2}`;
    player1turn = false;
    player2turn = true;
    shuffleCard();
    colorSet();
})

//GO BACK AND CHOOSE SELECTED
choose.addEventListener("click", () => {
    hide(content1);
    hide(content2);
    show(text);
    shuffleCard();
    score1P1 = 0;
    score1P2 = 0;
    score2P1 = 0;
    score2P2 = 0;
})

//TIMER
function initTimer() {
    timeCount++;
    timeTag.innerHTML = timeCount;
}

//CARD FLIPPING
function flipCard({ target: clickedCard }) {
    if (!isPlaying) {
        isPlaying = true;
        timer = setInterval(initTimer, 1000);
    }

    //IF YOU'RE NOT CLICKING THE SAME ELEMENT TWICE, IT CONTINUES AND THE ELEMENT FLIPS
    if (clickedCard !== cardOne && !disableDeck && timeCount >= 0) {
        if (player1turn) {
            flips1++;
        }
        else {
            flips2++;
        }

        //IF 3X4 SELECTED
        if(game1Started){
            flipsTag1_3x4.innerHTML = `Flips: ${flips1}`;
            flipsTag2_3x4.innerHTML = `Flips: ${flips2}`;
        }

        //IF 4X4 SELECTED
        else if(game2Started){
            flipsTag1_4x4.innerHTML = `Flips: ${flips1}`;
            flipsTag2_4x4.innerHTML = `Flips: ${flips2}`;
        }

        clickedCard.classList.add("flip");
        if (!cardOne) {
            return cardOne = clickedCard;
        }
        cardTwo = clickedCard;
        disableDeck = true;
        let cardOneImg = cardOne.querySelector(".back-view img").src,
            cardTwoImg = cardTwo.querySelector(".back-view img").src;
        matchCards(cardOneImg, cardTwoImg);
    }
}

function matchCards(img1, img2) {
    if (player1turn == true) {
        player2turn = true;
        player1turn = false;
    }
    else {
        player2turn = false;
        player1turn = true;
    }
    if (img1 === img2) {
        matchedCard++;

        if (game1Started) {
            if (player1turn) {
                score1P2++;
                playerScore2_3x4.innerHTML = `Matches found : ${score1P2}`;
            }
            else {
                score1P1++;
                playerScore1_3x4.innerHTML = `Matches found : ${score1P1}`;
            }

        }
        else {
            if (player1turn) {
                score2P2++;
                playerScore2_4x4.innerHTML = `Matches found : ${score2P2}`;
            }
            else {
                score2P1++;
                playerScore1_4x4.innerHTML = `Matches found : ${score2P1}`;
            }
        }

        //WIN/DRAW CONDITIONS FOR 3X4
        if (game1Started == true) {
            if (matchedCard == 6 ) {
                if(score1P1==score1P2){
                     playerScore1_3x4.innerHTML =playerScore2_3x4='Draw';
                }
                else if(score1P1>score1P2)
                {
                    playerScore1_3x4.innerHTML=` PLAYER 1 WINS!`;
                    playerScore1_3x4.style.color='#97dffc';
                }
                else if(score1P1<score1P2)
                {
                    playerScore2_3x4.innerHTML=` PLAYER 2 WINS!`;
                    playerScore2_3x4.style.color='#97dffc';
                }
                return clearInterval(timer);
            }
        }

        //WIN/DRAW CONDITIONS FOR 4X4
        else if (game2Started == true) {
            if (matchedCard == 8 ) {
                if(score2P1==score2P2){
                    playerScore1_4x4.innerHTML =playerScore2_4x4='Draw';
               }
               else if(score2P1>score2P2)
               {
                   playerScore1_4x4.innerHTML=` PLAYER 1 WINS!`;
                   playerScore2_4x4.style.color='#97dffc';

               }
               else if(score2P1<score2P2)
               {
                   playerScore2_4x4.innerHTML=` PLAYER 2 WINS!`;
                   playerScore2_4x4.style.color='#97dffc';
               }
                return clearInterval(timer);
            }
        }

        cardOne.removeEventListener("click", flipCard);
        cardTwo.removeEventListener("click", flipCard);
        cardOne = cardTwo = "";
        return disableDeck = false;
    }

     //THE CARD SHAKES IF A MATCH IS NOT FOUND
    setTimeout(() => {
        cardOne.classList.add("shake");
        cardTwo.classList.add("shake");
    }, 400);

    setTimeout(() => {
        cardOne.classList.remove("shake", "flip");
        cardTwo.classList.remove("shake", "flip");
        cardOne = cardTwo = "";
        disableDeck = false;
    }, 1200);

}

function shuffleCard() {
    timeCount = minTime;
    flips1 = matchedCard = 0;
    flips2 = matchedCard = 0;
    cardOne = cardTwo = "";
    clearInterval(timer);
    timeTag.innerHTML = timeCount;
    if(game1Started){
         flipsTag1_3x4.innerHTML = `Flips: ${flips1}`;
         flipsTag2_3x4.innerHTML = `Flips: ${flips2}`;
    }

    else{
        flipsTag1_4x4.innerHTML = `Flips: ${flips1}`;
         flipsTag2_4x4.innerHTML = `Flips: ${flips2}`;
    }
   
    disableDeck = isPlaying = false;

    //ARRAYS' NUMBER OF ELEMENTS= NUMBER OF ELEMENTS IN THE GAME
    let arr1 = [1, 2, 3, 4, 5, 6, 1, 2, 3, 4, 5, 6];
    let arr2 = [1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 6, 7, 8];
    arr1.sort(() => Math.random() > 0.5 ? 1 : -1);
    arr2.sort(() => Math.random() > 0.5 ? 1 : -1);

    cards.forEach((card, index) => {
        card.classList.remove("flip");
        let imgTag = card.querySelector(".back-view img");
        setTimeout(() => {
            imgTag.src = `images/img-${arr1[index]}.png`;
        }, 500);
        card.addEventListener("click", flipCard);
    });

    cards1.forEach((card1, index) => {
        card1.classList.remove("flip");
        
        //RANDOMLY CHOOSING ELEMENTS
        let imgTag = card1.querySelector(".back-view img");
        setTimeout(() => {
            imgTag.src = `images/img-${arr2[index]}.png`;
        }, 500);
        card1.addEventListener("click", flipCard);
    });
}

//REFRESH BUTTONS
refreshBtn1.addEventListener("click", shuffleCard);

refreshBtn1.addEventListener("click", () => {
    score1P1 = 0;
    score1P2 = 0;
    score2P1 = 0;
    score2P2 = 0;

    if(game1Started){
        playerScore1_3x4.innerHTML = `Matches found:${score1P1}`;
        playerScore2_3x4.innerHTML = `Matches found:${score1P2}`;
    }
    else if(game2Started){
        playerScore1_4x4.innerHTML = `Matches found:${score2P1}`;
        playerScore2_4x4.innerHTML = `Matches found:${score2P2}`;
    }
    colorSet();
})

refreshBtn2.addEventListener("click", shuffleCard);

refreshBtn2.addEventListener("click", () => {
    score1P1 = 0;
    score1P2 = 0;
    score2P1 = 0;
    score2P2 = 0;

    if(game1Started){
        playerScore1_3x4.innerHTML = `Matches found:${score1P1}`;
        playerScore2_3x4.innerHTML = `Matches found:${score1P2}`;
    }
    else if(game2Started){
        playerScore1_4x4.innerHTML = `Matches found:${score2P1}`;
        playerScore2_4x4.innerHTML = `Matches found:${score2P2}`;
    }

})
cards.forEach(card => {
    card.addEventListener("click", flipCard);
});

cards1.forEach(card1 => {
    card1.addEventListener("click", flipCard);
    colorSet();
});