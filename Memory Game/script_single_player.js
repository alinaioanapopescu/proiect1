let cards = document.querySelectorAll(".card");
let cards1 = document.querySelectorAll(".card1");

let timeTag1 = document.querySelectorAll(".time b")[0];
let timeTag2 = document.querySelectorAll(".time b")[1];

let flipsTag1 = document.querySelectorAll(".flips b")[0];
let flipsTag2 = document.querySelectorAll(".flips b")[1];

let refreshBtn1 = document.querySelectorAll(".details button")[0];
let refreshBtn2 = document.querySelectorAll(".details button")[1];

let playerScore1 = document.getElementById("single-player-text1");
let playerScore2 = document.getElementById("single-player-text2");

let gameMode1 = document.getElementById("3x4");
let gameMode2 = document.getElementById("4x4");

let content1 = document.getElementById("board3");
let content2 = document.getElementById("board4");

let text = document.getElementById("choose-game-mode");
let choose = document.getElementById("go-back-choose-btn");

let minTime = 0;
let timeCount = minTime;

let flips = 0;
let matchedCard = 0;
let disableDeck = false;
let isPlaying = false;
let cardOne, cardTwo, timer;

let score1 = 0;
let score2 = 0;

let game1Started = false;
let game2Started = false;

const show = elem => {
    elem.classList.remove('hide');
}

const hide = elem => {
    elem.classList.add('hide');
}
//SET FONT COLORS AT THE START
function colorSet(){
    playerScore1.style.color='white';
    playerScore2.style.color='white';
}
//3x4 MODE SELECTED
gameMode1.addEventListener("click", () => {
    show(content1);
    hide(text);
    show(choose);
    score1 = 0;
    game1Started = true;
    playerScore1.innerHTML = `Matches found:${score1}`;
    colorSet();
})

//4X4 MODE SELECTED 
gameMode2.addEventListener("click", () => {
    show(content2);
    hide(text);
    show(choose);
    score2 = 0;
    game2Started = true;
    playerScore2.innerText = `Matches found:${score2}`;
    console.log(playerScore2.innerHTML);
    colorSet();
})

//GO BACK AND CHOOSE SELECTED
choose.addEventListener("click", () => {
    hide(content1);
    hide(content2);
    show(text);
    shuffleCard();
    score1 = 0;
    score2 = 0;
    playerScore1.innerText = `Matches found:${score1}`;
    playerScore2.innerText = `Matches found:${score2}`;
})

//TIMER
function initTimer() {
    timeCount++;
    timeTag1.innerText = timeCount;
    timeTag2.innerText = timeCount;
}

//CARD FLIPPING
function flipCard({ target: clickedCard }) {
    if (!isPlaying) {
        isPlaying = true;
        timer = setInterval(initTimer, 500);
    }

    //IF YOU'RE NOT CLICKING THE SAME ELEMENT TWICE, IT CONTINUES AND THE ELEMENT FLIPS
    if (clickedCard !== cardOne && !disableDeck && timeCount >= 0) {
        flips++;
        flipsTag1.innerText = flips;
        flipsTag2.innerText = flips;
        clickedCard.classList.add("flip");
        if (!cardOne) {
            return cardOne = clickedCard;
        }
        cardTwo = clickedCard;
        disableDeck = true;
        let cardOneImg = cardOne.querySelector(".back-view img").src,
            cardTwoImg = cardTwo.querySelector(".back-view img").src;
        matchCards(cardOneImg, cardTwoImg);
    }
}

function matchCards(img1, img2) {
    if (img1 === img2) {
        matchedCard++;
        //IF 3X4 SELECTED
        if (game1Started == true) {
            score1++;
            playerScore1.innerText = `Matches found:${score1}`;
        }
        //IF 4X4 SELECTED
        else if (game2Started == true) {
            score2++;
            playerScore2.innerText = `Matches found:${score2}`;
        }
        //WIN CONDITION 3X4
        if (game1Started == true) {
            if (matchedCard == 6 && timeCount > 0) {
                playerScore1.innerText = `YOU WIN!`;
                playerScore1.style.color='#97dffc'
                return clearInterval(timer);
            }
        }
        //WIN CONDITION 4X4
        else if (game2Started == true) {
            if (matchedCard == 6 && timeCount > 0) {
                playerScore2.innerText = `YOU WIN!`;
                playerScore1.style.color='#97dffc'
                return clearInterval(timer);
            }
        }

        cardOne.removeEventListener("click", flipCard);
        cardTwo.removeEventListener("click", flipCard);
        cardOne = cardTwo = "";
        return disableDeck = false;
    }

    //THE CARD SHAKES IF A MATCH IS NOT FOUND
    setTimeout(() => {
        cardOne.classList.add("shake");
        cardTwo.classList.add("shake");
    }, 400);

    setTimeout(() => {
        cardOne.classList.remove("shake", "flip");
        cardTwo.classList.remove("shake", "flip");
        cardOne = cardTwo = "";
        disableDeck = false;
    }, 1200);
}

function shuffleCard() {
    timeCount = minTime;
    flips = matchedCard = 0;
    cardOne = cardTwo = "";
    clearInterval(timer);

    timeTag1.innerText = timeCount;
    timeTag2.innerText = timeCount;

    flipsTag1.innerText = flips;
    flipsTag2.innerText = flips;
    disableDeck = isPlaying = false;

    //ARRAYS' NUMBER OF ELEMENTS= NUMBER OF ELEMENTS IN THE GAME
    let arr1 = [1, 2, 3, 4, 5, 6, 1, 2, 3, 4, 5, 6];
    let arr2 = [1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 6, 7, 8];
    arr1.sort(() => Math.random() > 0.5 ? 1 : -1);
    arr2.sort(() => Math.random() > 0.5 ? 1 : -1);

    cards.forEach((card, index) => {
        card.classList.remove("flip");
        let imgTag = card.querySelector(".back-view img");
        setTimeout(() => {

            //RANDOMLY CHOOSING ELEMENTS
            imgTag.src = `images/img-${arr1[index]}.png`;
        }, 500);
        card.addEventListener("click", flipCard);
    });

    cards1.forEach((card1, index) => {
        card1.classList.remove("flip");
        let imgTag = card1.querySelector(".back-view img");
        setTimeout(() => {
            imgTag.src = `images/img-${arr2[index]}.png`;
        }, 500);
        card1.addEventListener("click", flipCard);
    });
}

shuffleCard();

//REFRESH BUTTONS
refreshBtn1.addEventListener("click", shuffleCard);

refreshBtn1.addEventListener("click", () => {
    score1 = 0;
    score2 = 0;
    playerScore1.innerText = `Matches found:${score1}`;
    playerScore2.innerText = `Matches found:${score2}`;
    colorSet();
})

refreshBtn2.addEventListener("click", shuffleCard);

refreshBtn2.addEventListener("click", () => {
    score1 = 0;
    score2 = 0;
    playerScore1.innerText = `Matches found:${score1}`;
    playerScore2.innerText = `Matches found:${score2}`;
    colorSet();
})
cards.forEach(card => {
    card.addEventListener("click", flipCard);
});

cards1.forEach(card1 => {
    card1.addEventListener("click", flipCard);
});