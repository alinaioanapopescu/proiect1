function calculateSupply(currentAge,amountperDay){
    const maxAge=80;
    let estimatedAmount=Math.round((maxAge-currentAge)*365*amountperDay);
    console.log(`You will need ${estimatedAmount} to last you until the ripe old age of ${maxAge}`);
}

calculateSupply(70,1.9);
calculateSupply(60,2);
calculateSupply(50,3);