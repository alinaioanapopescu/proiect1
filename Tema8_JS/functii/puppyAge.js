function calculateDogAge(puppysAge,humansAge) {
    dogYears = puppysAge * 7;
    humansAge=dogYears/7;
    console.log(`Your doggie is ${dogYears} years old in dog years!`);
    console.log(`Your doggie is ${humansAge} years old in human years!`);
}

calculateDogAge(3);
calculateDogAge(10);
calculateDogAge(7);