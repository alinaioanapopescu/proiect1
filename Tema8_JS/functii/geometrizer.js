const pi=3.141592;

function calcCircumference(radius){
    let circumference=2*pi*radius;
    console.log(`The circumference is ${circumference}`);
}

function calcArea(radius){
    let area=pi*Math.pow(radius,2);
    console.log(`The area is ${area}`);
}

calcCircumference(3);
calcArea(3);