function celsiusToFahrenheit() {
    let celsiusTemperature = 30;
    celsiusConverted = celsiusTemperature * 1.8 + 32;
    console.log(`${celsiusTemperature} °C is ${celsiusConverted} °F`);
}

function fahrenheitToCelsius() {

    let fahrenheitTemperature = 86;
    let celsiusTemperature = (fahrenheitTemperature - 32) / 1.8;
    console.log(`${fahrenheitTemperature} °F is ${celsiusTemperature} °C`);

}

celsiusToFahrenheit();
fahrenheitToCelsius();