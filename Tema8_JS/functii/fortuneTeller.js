function tellFortune(nbOfChildren,partnersName,geographicLocation,jobTitle){
    console.log(`You will be a ${jobTitle} in  ${geographicLocation} and married to ${partnersName} with  ${nbOfChildren} kids.`);
}

tellFortune(2,"John","America", "programmer");
tellFortune(3,"X","India", "doctor");
tellFortune(4,"Y","China", "singer");