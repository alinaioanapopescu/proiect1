function squareNumber(number){
    let square=Math.pow(number,2);
    console.log(`The result of squaring the number ${number} is ${square}.`)
    return square;
}

function halfNumber(number){
    let half=number/2;
    console.log(`Half of ${number} is ${half}`);
    return half;
}

// const pi=3.141592;
function areaOfcircle(radius){
    let area=pi*Math.pow(radius,2);
    console.log(`The area for a circle with radius ${radius} is ${area}.`);
    let roundedResult=area.toFixed(2);
    console.log(`Rounded result is ${roundedResult}`);
    return area;
}

function operation(number){
    let half=halfNumber(number);
    let square=squareNumber(half);
    let area=areaOfcircle(square);
    let percentage=(square/area)*100;
}

squareNumber(3);
halfNumber(5);
areaOfcircle(2);