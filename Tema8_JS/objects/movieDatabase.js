let movie={
    title:'Puff the magic dragon',
    duration:30,
    stars:['Puff', 'Jackie', 'Living Sneezes']
};

function print(movie){
    console.log(`${movie.title} lasts ${movie.duration} minutes, \nStars: `);
    for(let i=0;i<movie.stars.length;i++)
        console.log(movie.stars[i]);

}

print(movie);