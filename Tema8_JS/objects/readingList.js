let books=[
    {
        title:'x',
        author:'author1',
        alreadyRead:true
    },
    {
        title:'y',
        author:'author2',
        alreadyRead:false
    }
];

for(let i=0;i<books.length;i++){
    if(books[i].alreadyRead==true)
        {
            console.log( `You already read ${books[i].title} by ${books[i].author}`)
        }
    else console.log( `You still need to read ${books[i].title} by ${books[i].author}`);
}